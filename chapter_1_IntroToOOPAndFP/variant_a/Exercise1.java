package javabook.chapter_1_IntroToOOPAndFP.variant_a;
import java.util.Scanner;

public class Exercise1 {
  public static void main(String[] args) {
    System.out.print("Введите своё имя: ");

    Scanner scan = new Scanner(System.in);
    String name = scan.next();

    System.out.println("Добрый день, " + name);
    
    scan.close();
  }
}
