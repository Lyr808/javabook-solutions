package javabook.chapter_1_IntroToOOPAndFP.variant_a;

public class Exercise2 {
  public static void main(String[] args){
    for (int i = args.length - 1; i >= 0; i--) {
      System.out.println("Argument " + i + " :" + args[i]);
    }
  }
}
