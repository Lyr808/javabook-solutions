package javabook.chapter_1_IntroToOOPAndFP.variant_a;
import java.util.Scanner;
import java.util.Random;

public class Exercise3 {
  public static void main(String[] args) {
    System.out.print("Определите количество случайных чисел для вывода на экран: ");

    Random rand = new Random();
    Scanner scan = new Scanner(System.in);
    
    int num = scan.nextInt();

    System.out.println("Вывод случайных чисел с переходом на новую строку:");
    for (int i = num; i > 0; i--) {
      System.out.println(rand.nextInt());
    }
    System.out.println("Вывод случайных чисел без перехода на новую строку:");
    for (int i = num; i > 0; i--) {
      System.out.print(rand.nextInt() + " ");
    }

    scan.close();
  }
}
