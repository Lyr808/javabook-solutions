package javabook.chapter_1_IntroToOOPAndFP.variant_a;
import java.util.Scanner;

public class Exercise4 {
  public static void main(String[] args) {
    
    String psswrd = "4gh67SqwMV";
    Scanner scan = new Scanner(System.in);

    System.out.println("Введите пароль: ");

    String userPsswrd = scan.next();

    if (userPsswrd.equals(psswrd)) {
      System.out.println("Вы ввели правильный пароль");
    } else {
      System.out.println("Неверно введён пароль");
    }

    scan.close();
  }
}
