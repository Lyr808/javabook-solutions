package javabook.chapter_1_IntroToOOPAndFP.variant_a;

public class Exercise5 {
  public static void main(String[] args) {
    int sum = 0;
    int mult = 1;

    for (int i = 0; i < args.length; i++) {
      sum += Integer.parseInt(args[i]);
      mult *= Integer.parseInt(args[i]);
    }

    System.out.println("Сумма введённых аргументов командной строки: " + sum);
    System.out.println("Произведение введённых аргументов командной строки: " + mult);
  }
}
