package javabook.chapter_2_DataTypesAndOperators.variant_b;

public class Exercise1 {
  public static void main(String[] args) {

    int multSize = 9;

    System.out.print("*");
    for (int i = 1; i <= multSize; i++) {
      System.out.print("\t" + i);
    }
    System.out.println();

    for (int i = 1; i <= multSize; i++) {
      System.out.print(i + "\t");
      for (int j = 1; j <= multSize; j++) {
        System.out.print(i * j + "\t");
      }
      System.out.print("\n");
    }    
  }
}