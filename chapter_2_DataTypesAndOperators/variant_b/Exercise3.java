package javabook.chapter_2_DataTypesAndOperators.variant_b;
import java.util.Scanner;

public class Exercise3 {
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);

    System.out.print("Введите число: ");
    int userValue = scan.nextInt();

    System.out.print("Введите минимальное значение интервала: ");
    int minIntervalValue = scan.nextInt();

    System.out.print("Введите максимальное значение интервала: ");
    int maxIntervalValue = scan.nextInt();

    if ((userValue > minIntervalValue) && (userValue <= maxIntervalValue)) {
      System.out.println(userValue + " относится к интервалу (" + minIntervalValue + ", " + maxIntervalValue + "]"); 
    } else {
      System.out.println(userValue + " не относится к интервалу (" + minIntervalValue + ", " + maxIntervalValue + "]");
    }

    if ((userValue >= minIntervalValue) && (userValue < maxIntervalValue)) {
      System.out.println(userValue + " относится к интервалу [" + minIntervalValue + ", " + maxIntervalValue + ")"); 
    } else {
      System.out.println(userValue + " не относится к интервалу [" + minIntervalValue + ", " + maxIntervalValue + ")");
    }

    if ((userValue > minIntervalValue) && (userValue < maxIntervalValue)) {
      System.out.println(userValue + " относится к интервалу (" + minIntervalValue + ", " + maxIntervalValue + ")"); 
    } else {
      System.out.println(userValue + " не относится к интервалу (" + minIntervalValue + ", " + maxIntervalValue + ")");
    }

    if ((userValue >= minIntervalValue) && (userValue <= maxIntervalValue)) {
      System.out.println(userValue + " относится к интервалу [" + minIntervalValue + ", " + maxIntervalValue + "]"); 
    } else {
      System.out.println(userValue + " не относится к интервалу [" + minIntervalValue + ", " + maxIntervalValue + "]");
    }

    scan.close();
  }
}
