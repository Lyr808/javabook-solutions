package javabook.chapter_2_DataTypesAndOperators.variant_b;

public class Exercise4 {
  public static void main(String[] args) {
    for (int i = 1; i <= 100; i++) {
      if ((i % 3) == 0) {
        System.out.println(i);
      }
    }
  }
}
