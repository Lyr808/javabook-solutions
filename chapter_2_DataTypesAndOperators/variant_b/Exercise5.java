package javabook.chapter_2_DataTypesAndOperators.variant_b;

public class Exercise5 {
  public static void main(String[] args) {
    String num = "129";
    String binaryNum = Integer.toString(Integer.parseInt(num, 10), 2);

    int counter = 0;
    for (int i = 0; i < binaryNum.length() - 1; i++) {
      if (binaryNum.charAt(i) == '0') counter += 1;
    }

    System.out.println("В бинарном представлении числа " + num + " находится " + counter + " значащих нулей.");
  }
}
