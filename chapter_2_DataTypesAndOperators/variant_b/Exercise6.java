package javabook.chapter_2_DataTypesAndOperators.variant_b;

public class Exercise6 {
  public static void main(String[] args) {
    String num;
    for (int i = 2; i <= 36; i++) {
      num = Integer.toString(Integer.parseInt("81", 10), i);
      if (num.equals("100")) {
        System.out.print("Число 81 по основанию " + i + ": ");
        System.out.println(num);
        System.out.println(
          "\nОтвет: десятичное число 81 записывается в виде 100 в " +
          i + "-ой системе счисления"
          );
        break;
      } else {
        System.out.print("Число 81 по основанию " + i + ": ");
        System.out.println(num);
      }
    }
  }
}
