package javabook.chapter_2_DataTypesAndOperators.variant_b;
import java.util.Scanner;

public class Exercise7 {
  public static void main(String[] args) {
      System.out.println("Введите десятичное число: ");
      Scanner scan = new Scanner(System.in);
      String num = scan.next();

      System.out.println("Выберите систему счисления:");
      int userNotation = scan.nextInt();
      System.out.println(Integer.toString(Integer.parseInt(num, 10), userNotation));
    
      scan.close();
  }
}
