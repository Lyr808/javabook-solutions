package javabook.chapter_2_DataTypesAndOperators.variant_b;
import java.util.Scanner;

public class Exercise8 {
  public static void main(String[] args) {
    System.out.println("Введите число: ");
    Scanner scan = new Scanner(System.in);
    String num = scan.next();

    System.out.println("Выберите исходную систему счисления:");
    int sourceNotation = scan.nextInt();

    System.out.println("Выберите конечную систему счисления:");
    int destinationNotation = scan.nextInt();

    System.out.println(Integer.toString(Integer.parseInt(num, sourceNotation), destinationNotation));
  
    scan.close();
}
}
