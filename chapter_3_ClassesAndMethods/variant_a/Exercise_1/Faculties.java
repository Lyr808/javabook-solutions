package javabook.chapter_3_ClassesAndMethods.variant_a.Exercise_1;

public class Faculties {
  public static final String COMPUTER_SCIENCE = "Факультет компьютерных наук";
  public static final String WORLD_POLITICS   = "Факультет мировой экономики и мировой политики";
  public static final String ECONOMIC_SCIENCE = "Факультет экономических наук";
}