package javabook.chapter_3_ClassesAndMethods.variant_a.Exercise_1;

public class Student {
  private int id;
  private String lastName;
  private String firstName;
  private String middleName;
  private String homeAddress;
  private String phoneNumber;
  private String facultyName;
  private String courseName;
  private String studyGroup;
  private String dateOfBirth;

  public void setStudentID(int id) {
    this.id = id;
  }

  public void setStudentLastName(String lastName) {
    this.lastName = lastName;
  }

  public void setStudentFirstName(String firstName) {
    this.firstName = firstName;
  }

  public void setStudentMiddleName(String middleName) {
    this.middleName = middleName;
  }

  public void setStudentHomeAddress(String homeAddress) {
    this.homeAddress = homeAddress;
  }

  public void setStudentPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public void setStudentFacultyName(String facultyName) {
    this.facultyName = facultyName;
  }

  public void setStudentCourseName(String courseName) {
    this.courseName = courseName;
  }

  public void setStudentStudyGroup(String studyGroup) {
    this.studyGroup = studyGroup;
  }

  public void setStudentDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public int getStudentID() {
    return id;
  }

  public String getLastName() {
    return lastName;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getMiddleName() {
    return middleName;
  }

  public String getHomeAddress() {
    return homeAddress;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public String getFacultyName() {
    return facultyName;
  }

  public String getCourseName() {
    return courseName;
  }

  public String getStudyGroup() {
    return studyGroup;
  }

  public String getDateOfBirth() {
    return dateOfBirth;
  }

  @Override
  public String toString () {
    final StringBuilder sb = new StringBuilder(
      "Идентификатор студента: ").append(this.id).append("\n");
    sb.append(
      "Фамилия:                ").append(this.lastName).append("\n");
    sb.append(
      "Имя:                    ").append(this.firstName).append("\n");
    sb.append(
      "Отчество:               ").append(this.middleName).append("\n");
    sb.append(
      "Дата рождения:          ").append(this.dateOfBirth).append("\n");
    sb.append(
      "Факультет:              ").append(this.facultyName).append("\n");
    sb.append(
      "Курс:                   ").append(this.courseName).append("\n");
    sb.append(
      "Группа:                 ").append(this.studyGroup).append("\n");
    sb.append(
      "Адрес проживания:       ").append(this.homeAddress).append("\n");
    sb.append(
      "Номер телефона:         ").append(this.phoneNumber).append("\n");
    return sb.toString();
  }
}