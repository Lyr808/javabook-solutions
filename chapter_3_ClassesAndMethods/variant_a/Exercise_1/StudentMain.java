package javabook.chapter_3_ClassesAndMethods.variant_a.Exercise_1;
import static javabook.chapter_3_ClassesAndMethods.variant_a.Exercise_1.Faculties.*;
import static javabook.chapter_3_ClassesAndMethods.variant_a.Exercise_1.Students.*;

public class StudentMain {
  public static void main(String[] args) {
    Student stud1 = new Student();
    stud1.setStudentID(1);
    stud1.setStudentLastName("Волков");
    stud1.setStudentFirstName("Николай");
    stud1.setStudentMiddleName("Сергеевич");
    stud1.setStudentHomeAddress("Омск, ул. Чернышева, д. 54, кв. 10");
    stud1.setStudentPhoneNumber("+79145672322");
    stud1.setStudentFacultyName(COMPUTER_SCIENCE);
    stud1.setStudentCourseName("Анализ данных в финансах");
    stud1.setStudentStudyGroup("ИТ-1");
    stud1.setStudentDateOfBirth("22.04.1998");

    Student stud2 = new Student();
    stud2.setStudentID(2);
    stud2.setStudentLastName("Юрченко");
    stud2.setStudentFirstName("Степан");
    stud2.setStudentMiddleName("Вячеславович");
    stud2.setStudentHomeAddress("Москва, ул. Б.Новодмитровская, д. 1, кв. 46");
    stud2.setStudentPhoneNumber("+74957922132");
    stud2.setStudentFacultyName(COMPUTER_SCIENCE);
    stud2.setStudentCourseName("Анализ данных в финансах");
    stud2.setStudentStudyGroup("ИТ-1");
    stud2.setStudentDateOfBirth("18.02.1990");

    Student stud3 = new Student();
    stud3.setStudentID(3);
    stud3.setStudentLastName("Сорос");
    stud3.setStudentFirstName("Георг");
    stud3.setStudentMiddleName("Петрович");
    stud3.setStudentHomeAddress("Москва, ул. Твардовского, д. 63, кв. 32");
    stud3.setStudentPhoneNumber("+74195678901");
    stud3.setStudentFacultyName(WORLD_POLITICS);
    stud3.setStudentCourseName("Мировая экономика");
    stud3.setStudentStudyGroup("ЭК-1");
    stud3.setStudentDateOfBirth("12.08.1930");

    Student stud4 = new Student();
    stud4.setStudentID(4);
    stud4.setStudentLastName("Захаров");
    stud4.setStudentFirstName("Артур");
    stud4.setStudentMiddleName("Николаевич");
    stud4.setStudentHomeAddress("Москва, ул. Палиха, д. 81, кв. 82");
    stud4.setStudentPhoneNumber("+74954094407");
    stud4.setStudentFacultyName(WORLD_POLITICS);
    stud4.setStudentCourseName("Мировая экономика");
    stud4.setStudentStudyGroup("ЭК-1");
    stud4.setStudentDateOfBirth("26.06.1989");

    Student stud5 = new Student();
    stud5.setStudentID(5);
    stud5.setStudentLastName("Власов");
    stud5.setStudentFirstName("Константин");
    stud5.setStudentMiddleName("Валентинович");
    stud5.setStudentHomeAddress("Москва, ул. 4-я Новые Сады, д. 2, кв. 94");
    stud5.setStudentPhoneNumber("+74952400042");
    stud5.setStudentFacultyName(ECONOMIC_SCIENCE);
    stud5.setStudentCourseName("Экономика и статистика");
    stud5.setStudentStudyGroup("ЭК-2");
    stud5.setStudentDateOfBirth("24.08.1990");

    Student stud6 = new Student();
    stud6.setStudentID(6);
    stud6.setStudentLastName("Шевченко");
    stud6.setStudentFirstName("Глеб");
    stud6.setStudentMiddleName("Филиппович");
    stud6.setStudentHomeAddress("Москва, ул. 5-я Ямского Поля, д. 90, кв. 63");
    stud6.setStudentPhoneNumber("+74956178569");
    stud6.setStudentFacultyName(ECONOMIC_SCIENCE);
    stud6.setStudentCourseName("Экономика и статистика");
    stud6.setStudentStudyGroup("ЭК-2");
    stud6.setStudentDateOfBirth("19.01.1987");

    Student[] studentsArray = getArrayOfStudents(
      stud1, 
      stud2,
      stud3,
      stud4,
      stud5,
      stud6
    );

    getStudentsByFaculty(studentsArray, COMPUTER_SCIENCE);
    getStudentsByFaculty(studentsArray, WORLD_POLITICS);
    getStudentsByFaculty(studentsArray, ECONOMIC_SCIENCE);

    getAllStudents(
      studentsArray, 
      COMPUTER_SCIENCE, 
      WORLD_POLITICS, 
      ECONOMIC_SCIENCE
    );

    getStudentsBornAfterSelectedYear(studentsArray, 1990);

    getStudentsByStudyGroup(studentsArray, "ИТ-1");
  }
}
