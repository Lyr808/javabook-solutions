package javabook.chapter_3_ClassesAndMethods.variant_a.Exercise_1;

public class Students {
  public static Student[] getArrayOfStudents(Student... students) {
    Student[] studentsArray = new Student[students.length];
    for (int i = 0; i < students.length; i++) {
      studentsArray[i] = students[i];
    }
    return studentsArray;
  }

  public static void getStudentsByFaculty(Student[] students, String facultyName) {
    System.out.println("Список студентов по запросу \"" + facultyName + "\":");
    for (int i = 0; i < students.length; i++) {
      if (students[i].getFacultyName().equals(facultyName)) {
        System.out.println(students[i].toString());
      }
    }
  }

  public static void getAllStudents(Student[] students, String... faculties) {
    System.out.println("Список студентов для каждого факультета и курса:");
    for (String faculty : faculties) {
      System.out.println(faculty + ":");
      for (int j = 0; j < students.length; j++) {
        if (students[j].getFacultyName().equals(faculty)) {
          System.out.println(students[j].toString());
        }
      }
    }
  }

  public static void getStudentsBornAfterSelectedYear(Student[] students, int year) {
    System.out.println("Список студентов, родившихся после " + year + " года:");
    for (int i = 0; i < students.length; i++) {
      int studentDateOfBirth = Integer.parseInt(students[i].getDateOfBirth().substring(6, 10));
      if (studentDateOfBirth > year) {
        System.out.println(students[i].toString());
      }
    }
  }

  public static void getStudentsByStudyGroup(Student[] students, String studyGroup) {
    System.out.println("Список студентов учебной группы " + studyGroup + ":");
    for (int i = 0; i < students.length; i++) {
      if (students[i].getStudyGroup().equals(studyGroup)) {
        System.out.println(students[i].toString());
      }
    }
  }
}