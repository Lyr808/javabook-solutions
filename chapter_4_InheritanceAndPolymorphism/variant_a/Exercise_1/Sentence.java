package javabook.chapter_4_InheritanceAndPolymorphism.variant_a.Exercise_1;

public class Sentence extends Word {
  private String sentence;

  public Sentence() {
    super();
  }

  public Sentence(String[] words) {
    getSentenceFromWords(words);
  }

  public Sentence(String[] words, String header) {
    getSentenceFromWords(words);
    addHeader(header);
  }

  private void getSentenceFromWords (String[] words) {
    String newSentence = "";
    for (String word : words) {
      newSentence = addWord(newSentence, word);
    }
    this.sentence = newSentence;
  }

  private void addHeader (String header) {
    final StringBuilder stringWithHeader = new StringBuilder()
      .append(this.sentence)
      .insert(0, header)
      .insert(header.length(), "\n\n")
    ;
    this.sentence = stringWithHeader.toString();
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder().append(this.sentence);
    return sb.toString();
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
      return true;
    }
    if (object == null || this.getClass() != object.getClass()) {
      return false;
    }
    if (this.hashCode() != object.hashCode()) {
      return false;
    }
    return object.toString() == sentence ? true : false;
  }

  @Override
  public int hashCode() {
    return 42 * (sentence != null ? sentence.hashCode() : 0);
  }
}
