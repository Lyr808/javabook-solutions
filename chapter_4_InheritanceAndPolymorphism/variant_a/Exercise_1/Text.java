package javabook.chapter_4_InheritanceAndPolymorphism.variant_a.Exercise_1;

public class Text extends Sentence {
  public static void main(String[] args) {
    String[] wrds = {"Hello", ", ", "World"};
    String header = "This is a header";

    Sentence sentence1 = new Sentence(wrds);
    Sentence sentence2 = new Sentence(wrds, header);

    System.out.println("Текст без заголовка:\n");
    System.out.println(sentence1.toString());
    System.out.println();

    System.out.println("Текст с заголовком:\n");
    System.out.println(sentence2.toString());
    System.out.println();

    System.out.println("Проверка двух ссылок на объекты на равенство:");
    System.out.println("sentence1 == sentence2: " + sentence1.equals(sentence2));
    System.out.println("sentence1 == sentence1: " + sentence1.equals(sentence1));
    System.out.println("Hash check for sentence1: " + sentence1.hashCode());
    System.out.println("Hash check for sentence2: " + sentence2.hashCode());
  }
}
