package javabook.chapter_4_InheritanceAndPolymorphism.variant_a.Exercise_1;

public class Word {
  protected final String addWord(String sentence, String word) {
    return sentence.concat(word);
  }
}
