package javabook.chapter_4_InheritanceAndPolymorphism.variant_a.Exercise_2;

public class Car extends Engine {
  public static void main(String[] args) {
    Car car = new Car();

    car.setFrontLeftWheel("Michelin");
    car.setFrontRightWheel("Continental");
    car.setBackLeftWheel("Goodyear");
    car.setBackRightWheel("Bridgestone");

    System.out.println("Car: Start running");
    car.startRunning();

    System.out.println("Current back left wheel: " + car.getBackLeftWheel());
    System.out.println("Change back left wheel");
    car.setBackLeftWheel("Yokohama");
    System.out.println("Current back left wheel: " + car.getBackLeftWheel());
  }
}
