package javabook.chapter_4_InheritanceAndPolymorphism.variant_a.Exercise_2;

public class Engine extends Wheel {
  private String runningStatus = "Not running";

  public final void startRunning() {
    if (
      getFrontLeftWheel()     != null | 
      getFrontRightWheel()    != null | 
      getBackLeftWheel()      != null | 
      getBackRightWheel()     != null
    ) {
      this.runningStatus = "Running";
      System.out.println("Running:" + isRunning());
    } else if (getFrontLeftWheel() == null) {
      System.out.println("Can't start the car, front left wheel isn't set");
      this.runningStatus = "Not running";
      System.out.println("Running:" + isRunning());
    } else if (getFrontRightWheel() == null) {
      System.out.println("Can't start the car, front right wheel isn't set");
      this.runningStatus = "Not running";
      System.out.println("Running:" + isRunning());
    } else if (getBackLeftWheel() == null) {
      System.out.println("Can't start the car, back left wheel isn't set");
      this.runningStatus = "Not running";
      System.out.println("Running:" + isRunning());
    } else if (getBackRightWheel() == null) {
      System.out.println("Can't start the car, back right wheel isn't set");
      this.runningStatus = "Not running";
      System.out.println("Running:" + isRunning());
    }
  }

  private boolean isRunning() {
    return runningStatus.equals("Not running") ? false : true;
  }
}
