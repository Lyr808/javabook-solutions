package javabook.chapter_4_InheritanceAndPolymorphism.variant_a.Exercise_2;

public class Wheel {
  private String frontLeftWheel;
  private String frontRightWheel;
  private String backLeftWheel;
  private String backRightWheel;

  public void setFrontLeftWheel(String wheel) {
    this.frontLeftWheel = wheel;
  }

  public void setFrontRightWheel(String wheel) {
    this.frontRightWheel = wheel;
  }

  public void setBackLeftWheel(String wheel) {
    this.backLeftWheel = wheel;
  }

  public void setBackRightWheel(String wheel) {
    this.backRightWheel = wheel;
  }

  public String getFrontLeftWheel() {
    return this.frontLeftWheel;
  }

  public String getFrontRightWheel() {
    return this.frontRightWheel;
  }

  public String getBackLeftWheel() {
    return this.backLeftWheel;
  }

  public String getBackRightWheel() {
    return this.backRightWheel;
  }
}