package javabook.chapter_5_InnerClasses.Exercise_1;

public class Notepad {
  private String noteDate;
  { noteDate = "21.02.2022"; }
  
  public class Entry {
    private String newEntry;

    public Entry(String newEntry) {
      final StringBuilder sb = new StringBuilder()
        .append("Date: " + noteDate + "\n")
        .append("Entry: " + newEntry);
      this.newEntry = sb.toString();
    }

    public void showEntry() {
      System.out.println(newEntry);
    }
  }
}