package javabook.chapter_5_InnerClasses.Exercise_1;

public class NotepadMain {
  public static void main(String[] args) {
    String entryString = "Do the dishes";
    String anotherEntryString = "Eat cake";

    Notepad.Entry newEntry = new Notepad().new Entry(entryString);
    Notepad.Entry anotherNewEntry = new Notepad().new Entry(anotherEntryString);

    newEntry.showEntry();
    System.out.println();
    anotherNewEntry.showEntry();
  }
}
