package javabook.chapter_6_InterfacesAndAnnotations.Exercise_1;

public abstract class AbstractEdition {
  protected int     editionPayment;
  protected String  editionContract;
  protected String  editionDescription;
  protected String  editionName;
  protected String  editionStatus;
  protected boolean contractMade = false;
  protected final String DECLINED_EDITION = "Declined";
  protected final String RESUMED_EDITION  = "Resumed";
  protected final String CLOSED_EDITION   = "Closed";
  protected final String OPENED_EDITION   = "Opened";
  protected final String TYPESET_EDITION  = "Typeset";
  protected final String PRINT_EDITION    = "Printed";
}
