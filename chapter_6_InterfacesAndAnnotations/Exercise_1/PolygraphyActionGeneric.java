package javabook.chapter_6_InterfacesAndAnnotations.Exercise_1;

public interface PolygraphyActionGeneric <T extends AbstractEdition> {
  void editEdition    (String name, String description);
  void makeContract   (String contract);
  void payForEdition  (int payment);
  void openEdition    ();
  void typesetEdition ();
  void printEdition   ();
  void declineEdition ();
  void resumeEdition  ();
  void closeEdition   ();
}
