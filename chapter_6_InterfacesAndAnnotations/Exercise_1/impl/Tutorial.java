package javabook.chapter_6_InterfacesAndAnnotations.Exercise_1.impl;
import javabook.chapter_6_InterfacesAndAnnotations.Exercise_1.AbstractEdition;
import javabook.chapter_6_InterfacesAndAnnotations.Exercise_1.PolygraphyActionGeneric;

public class Tutorial extends AbstractEdition implements PolygraphyActionGeneric<Tutorial> {
  @Override
  public void makeContract(String contract) {
    this.editionContract = contract;
    this.contractMade = true;
    System.out.println("Сделан контракт на учебное пособие " + this.editionName);
  }

  @Override
  public void editEdition(String name, String description) { 
    this.editionName = name;
    this.editionDescription = description;
  }

  @Override
  public void payForEdition(int payment) {
    this.editionPayment = payment;
  }

  @Override
  public void openEdition() {
    this.editionStatus = this.OPENED_EDITION;
  }

  @Override
  public void typesetEdition() {
    this.editionStatus = this.TYPESET_EDITION;
  }

  @Override
  public void printEdition() {
    this.editionStatus = this.PRINT_EDITION;
  }

  @Override
  public void declineEdition() {
    this.editionStatus = this.DECLINED_EDITION;
  }

  @Override
  public void closeEdition() {
    this.editionStatus = this.CLOSED_EDITION;
  }

  @Override
  public void resumeEdition() {
    this.editionStatus = this.RESUMED_EDITION;
  }
}